import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { MainComponent } from './layout/main/main.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';

//services
import { API } from './services/api';
import { AuthService } from './services/auth.service';
import { PhongBanService } from './services/phongban.service';
import { DanhsachphongbanComponent } from './pages/danhba/danhsachphongban/danhsachphongban.component';
import { ThemmoiphongbanComponent } from './pages/danhba/themmoiphongban/themmoiphongban.component';
import { ChinhsuaphongbanComponent } from './pages/danhba/chinhsuaphongban/chinhsuaphongban.component';
import { DanhsachchucvuComponent } from './pages/chucvu/danhsachchucvu/danhsachchucvu.component';
import { ThemmoichucvuComponent } from './pages/chucvu/themmoichucvu/themmoichucvu.component';
import { ChinhsuachucvuComponent } from './pages/chucvu/chinhsuachucvu/chinhsuachucvu.component';
import { DanhsachnhanvienComponent } from './pages/nhanvien/danhsachnhanvien/danhsachnhanvien.component';
import { ChinhsuanhanvienComponent } from './pages/nhanvien/chinhsuanhanvien/chinhsuanhanvien.component';
import { ThemmoinhanvienComponent } from './pages/nhanvien/themmoinhanvien/themmoinhanvien.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    DanhsachphongbanComponent,
    ThemmoiphongbanComponent,
    ChinhsuaphongbanComponent,
    DanhsachchucvuComponent,
    ThemmoichucvuComponent,
    ChinhsuachucvuComponent,
    DanhsachnhanvienComponent,
    ChinhsuanhanvienComponent,
    ThemmoinhanvienComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    API,
    AuthService,
    PhongBanService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
