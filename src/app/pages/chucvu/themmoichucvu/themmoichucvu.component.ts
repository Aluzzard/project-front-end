import { Component, OnInit } from '@angular/core';
import { ChucVu } from '../../../models/chucvu.model';
import { ChucVuService } from '../../../services/chucvu.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-themmoichucvu',
  templateUrl: './themmoichucvu.component.html',
  styleUrls: ['./themmoichucvu.component.scss']
})
export class ThemmoichucvuComponent implements OnInit {

 
  //model
  CurrentChucVu:ChucVu = new ChucVu();

  constructor(
    private phongbanService: ChucVuService,
    private _location: Location,
  ) {
   }

  ngOnInit(): void {
  
  }

  //lưu dữ liệu
  submitData(){
    //dữ liệu hợp lệ
    if(this.CurrentChucVu.formGroup.valid){
      var result:any;
      this.phongbanService
        .add(this.CurrentChucVu.tenChucVu)
        .subscribe((res) => {
          result = res;
        }, (err) => {
          alert('Thêm mới không thành công');
        }, () => {
          alert('Thêm mới thành công. Id: #' + result.object.data);
          this._location.back();
        });
    }
  }


}
