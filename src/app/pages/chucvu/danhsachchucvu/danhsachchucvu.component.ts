import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ChucVuService } from '../../../services/chucvu.service';

@Component({
  selector: 'app-danhsachchucvu',
  templateUrl: './danhsachchucvu.component.html',
  styleUrls: ['./danhsachchucvu.component.scss']
})
export class DanhsachchucvuComponent implements OnInit {

  danhsachchucvu:[];
  total:number = 0;

  constructor(
    private router: Router,
    private chucvuService: ChucVuService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  goToEdit(menu_link: string, id: number) {
    let nav: NavigationExtras = {
      state: {
        id: id,
      }
    }
    this.router.navigate([menu_link], nav);
  }

  loadData(){
    var result:any;

    this.chucvuService
      .listByUser()
      .subscribe((res) =>{
        result = res;
      }, (err) => {
        console.log(err);
      }, () => {

        this.danhsachchucvu = result.object.items;
        this.total = result.object.total;
      });
  }

  deleteItem(id:number){
    if(window.confirm('Bạn thực sự muốn xóa?')){
      this.chucvuService.delete(id)
      .subscribe((res) =>{
      }, (err) => {
        console.log(err);
      }, () => {
        alert('Xóa thành công');
        //reload data
        this.loadData();
      });
    }
  }
}
