import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhsachchucvuComponent } from './danhsachchucvu.component';

describe('DanhsachchucvuComponent', () => {
  let component: DanhsachchucvuComponent;
  let fixture: ComponentFixture<DanhsachchucvuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DanhsachchucvuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhsachchucvuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
