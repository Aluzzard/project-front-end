import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChinhsuachucvuComponent } from './chinhsuachucvu.component';

describe('ChinhsuachucvuComponent', () => {
  let component: ChinhsuachucvuComponent;
  let fixture: ComponentFixture<ChinhsuachucvuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChinhsuachucvuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChinhsuachucvuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
