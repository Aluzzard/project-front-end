import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChucVu } from '../../../models/chucvu.model';
import { ChucVuService } from '../../../services/chucvu.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-chinhsuachucvu',
  templateUrl: './chinhsuachucvu.component.html',
  styleUrls: ['./chinhsuachucvu.component.scss']
})
export class ChinhsuachucvuComponent implements OnInit {

  getChucVuID: number;
  currentID: any;
   //model
  CurrentChucVu:ChucVu = new ChucVu();

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private _location: Location,
    private chucvuService: ChucVuService
  ) { 
    this.route.queryParams.subscribe(params => {
      this.currentID = this.router.getCurrentNavigation().extras.state.id;
    });
    console.log(this.currentID);
    
    
  }

  ngOnInit() {
    this.loadData(this.currentID);
    console.log(this.currentID);
  }

  loadData(currentID){
    var result:any;

    this.chucvuService
      .getById(currentID)
      .subscribe((res) =>{
        result = res;
      }, (err) => {
        console.log(err);
      }, () => {
        this.CurrentChucVu.tenChucVu = result.object.tenChucVu;
        this.getChucVuID = result.object.id;
      });
  }

  //lưu dữ liệu
  submitData(){
    //dữ liệu hợp lệ
    if(this.CurrentChucVu.formGroup.valid){
      var result:any;
      this.chucvuService
        .edit(this.getChucVuID, this.CurrentChucVu.tenChucVu)
        .subscribe((res) => {
          result = res;
        }, (err) => {
          alert('Chỉnh sửa không thành công');
        }, () => {
          alert('Chỉnh sửa thành công #ID: ' + this.getChucVuID);
          this._location.back();
        });
    }
  }

}
