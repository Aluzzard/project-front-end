import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemmoinhanvienComponent } from './themmoinhanvien.component';

describe('ThemmoinhanvienComponent', () => {
  let component: ThemmoinhanvienComponent;
  let fixture: ComponentFixture<ThemmoinhanvienComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThemmoinhanvienComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemmoinhanvienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
