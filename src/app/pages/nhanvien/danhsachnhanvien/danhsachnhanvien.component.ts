import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NhanVienService } from '../../../services/nhanvien.service';

@Component({
  selector: 'app-danhsachnhanvien',
  templateUrl: './danhsachnhanvien.component.html',
  styleUrls: ['./danhsachnhanvien.component.scss']
})
export class DanhsachnhanvienComponent implements OnInit {

  danhsachnhanvien:[];
  total:number = 0;

  constructor(
    private router: Router,
    private nhanvienService: NhanVienService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  goToEdit(menu_link: string, id: number) {
    let nav: NavigationExtras = {
      state: {
        id: id,
      }
    }
    this.router.navigate([menu_link], nav);
  }

  loadData(){
    var result:any;

    this.nhanvienService
      .listByUser()
      .subscribe((res) =>{
        result = res;
      }, (err) => {
        console.log(err);
      }, () => {

        this.danhsachnhanvien = result.object.items;
        console.log(this.danhsachnhanvien);
        this.total = result.object.total;
      });
  }

  deleteItem(id:number){
    if(window.confirm('Bạn thực sự muốn xóa?')){
      this.nhanvienService.delete(id)
      .subscribe((res) =>{
      }, (err) => {
        console.log(err);
      }, () => {
        alert('Xóa thành công');
        //reload data
        this.loadData();
      });
    }
  }

}
