import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChinhsuaphongbanComponent } from './chinhsuaphongban.component';

describe('ChinhsuaphongbanComponent', () => {
  let component: ChinhsuaphongbanComponent;
  let fixture: ComponentFixture<ChinhsuaphongbanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChinhsuaphongbanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChinhsuaphongbanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
