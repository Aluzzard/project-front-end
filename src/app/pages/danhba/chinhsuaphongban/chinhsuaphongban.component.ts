import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PhongBan } from '../../../models/phongban.model';
import { PhongBanService } from '../../../services/phongban.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-chinhsuaphongban',
  templateUrl: './chinhsuaphongban.component.html',
  styleUrls: ['./chinhsuaphongban.component.scss']
})
export class ChinhsuaphongbanComponent implements OnInit {
  
  getPhongBanID: number;
  currentID: any;
   //model
  CurrentPhongBan:PhongBan = new PhongBan();

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private _location: Location,
    private phongbanService: PhongBanService
  ) { 
    this.route.queryParams.subscribe(params => {
      this.currentID = this.router.getCurrentNavigation().extras.state.id;
    });
  }

  ngOnInit() {
    this.loadData(this.currentID);
  }

  loadData(currentID){
    var result:any;

    this.phongbanService
      .getById(currentID)
      .subscribe((res) =>{
        result = res;
      }, (err) => {
        console.log(err);
      }, () => {
        this.CurrentPhongBan.tenPhongBan = result.object.tenPhongBan;
        this.getPhongBanID = result.object.id;
      });
  }

  //lưu dữ liệu
  submitData(){
    //dữ liệu hợp lệ
    if(this.CurrentPhongBan.formGroup.valid){
      var result:any;
      this.phongbanService
        .edit(this.getPhongBanID, this.CurrentPhongBan.tenPhongBan)
        .subscribe((res) => {
          result = res;
        }, (err) => {
          alert('Chỉnh sửa không thành công');
        }, () => {
          alert('Chỉnh sửa thành công #ID: ' + this.getPhongBanID);
          this._location.back();
        });
    }
  }
}
