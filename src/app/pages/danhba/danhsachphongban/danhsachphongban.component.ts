import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { PhongBanService } from '../../../services/phongban.service';

@Component({
  selector: 'app-danhsachphongban',
  templateUrl: './danhsachphongban.component.html',
  styleUrls: ['./danhsachphongban.component.scss']
})
export class DanhsachphongbanComponent implements OnInit {

  danhsachphongban:[];
  total:number = 0;

  constructor(
    private router: Router,
    private phongbanService: PhongBanService
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  goToEdit(menu_link: string, id: number) {
    let nav: NavigationExtras = {
      state: {
        id: id,
      }
    }
    this.router.navigate([menu_link], nav);
  }

  loadData(){
    var result:any;

    this.phongbanService
      .listByUser()
      .subscribe((res) =>{
        result = res;
      }, (err) => {
        console.log(err);
      }, () => {

        this.danhsachphongban = result.object.items;
        this.total = result.object.total;
      });
  }

  deleteItem(id:number){
    if(window.confirm('Bạn thực sự muốn xóa?')){
      this.phongbanService.delete(id)
      .subscribe((res) =>{
      }, (err) => {
        console.log(err);
      }, () => {
        alert('Xóa thành công');
        //reload data
        this.loadData();
      });
    }
  }

}
