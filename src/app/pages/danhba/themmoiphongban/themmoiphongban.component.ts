import { Component, OnInit } from '@angular/core';
import { PhongBan } from '../../../models/phongban.model';
import { PhongBanService } from '../../../services/phongban.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-themmoiphongban',
  templateUrl: './themmoiphongban.component.html',
  styleUrls: ['./themmoiphongban.component.scss']
})
export class ThemmoiphongbanComponent implements OnInit {

  //model
  CurrentPhongBan:PhongBan = new PhongBan();

  constructor(
    private phongbanService: PhongBanService,
    private _location: Location,
  ) {
   }

  ngOnInit(): void {
  
  }

  //lưu dữ liệu
  submitData(){
    //dữ liệu hợp lệ
    if(this.CurrentPhongBan.formGroup.valid){
      var result:any;
      this.phongbanService
        .add(this.CurrentPhongBan.tenPhongBan)
        .subscribe((res) => {
          result = res;
        }, (err) => {
          alert('Thêm mới không thành công');
        }, () => {
          alert('Thêm mới thành công. Id: #' + result.object.data);
          this._location.back();
        });
    }
  }

}
