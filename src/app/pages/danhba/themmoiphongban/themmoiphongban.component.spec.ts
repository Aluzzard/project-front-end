import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemmoiphongbanComponent } from './themmoiphongban.component';

describe('ThemmoiphongbanComponent', () => {
  let component: ThemmoiphongbanComponent;
  let fixture: ComponentFixture<ThemmoiphongbanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThemmoiphongbanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemmoiphongbanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
