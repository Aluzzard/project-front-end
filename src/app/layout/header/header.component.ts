import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { AuthGuard } from "../../guards/auth.guard";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: boolean;
  constructor(
    private authService: AuthService,
    private authGuard: AuthGuard
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.authGuard.check();
  }

  logout(){
    this.authService.logout();
  }

}
