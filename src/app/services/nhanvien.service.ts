import { Injectable } from '@angular/core';
import { API } from './api';

@Injectable({
  providedIn: 'root'
})
export class NhanVienService {

  constructor(
    private api: API
  ) { }

  listByUser() {
    return this.api.get(`/api/DanhBa/list-nhan-vien?PageNumber=1&PageSize=10&Keyword=&maPhongBan=0&maChucVu=0&trangThai=1`);
  }

  add(name: string) {
    return this.api.post('/api/DanhBa/add-chuc-vu', {
      "tenChucVu": name
    });
  }

  edit(id: number, name: string) {
    return this.api.put('/api/DanhBa/edit-chuc-vu', {
      "id": id,
      "tenChucVu": name
    });
  }

  delete(id:number){
    return this.api.delete('/api/DanhBa/delete-chuc-vu/'+id);
  }

  getById(id:number){
    return this.api.get('/api/DanhBa/get-chuc-vu/'+id);
  }
}
