import { Injectable } from '@angular/core';
import { API } from './api';

@Injectable({
  providedIn: 'root'
})
export class PhongBanService {

  constructor(
    private api: API
  ) { }

  listByUser() {
    return this.api.get('/api/DanhBa/list-phong-ban');
  }

  add(name: string) {
    return this.api.post('/api/DanhBa/add-phong-ban', {
      "tenPhongBan": name
    });
  }

  edit(id: number, name: string) {
    return this.api.put('/api/DanhBa/edit-phong-ban', {
      "id": id,
      "tenPhongBan": name
    });
  }

  delete(id:number){
    return this.api.delete('/api/DanhBa/delete-phong-ban/'+id);
  }

  getById(id:number){
    return this.api.get('/api/DanhBa/get-phong-ban/'+id);
  }
}
