"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.API = void 0;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var config_1 = require("../config");
var config_responseType = {
    'blob': 'blob',
    'arraybuffer': 'arraybuffer',
    'json': 'json',
    'text': 'text',
    'none': ''
};
var API = /** @class */ (function () {
    function API(http, router) {
        this.http = http;
        this.router = router;
        this.config = config_1.Config;
    }
    API.prototype.getToken = function () {
        var accessToken = localStorage.getItem('token') ? localStorage.getItem('token') : sessionStorage.getItem('token') || '';
        if (accessToken !== '') {
            return "Bearer " + accessToken;
        }
        else {
            return '';
        }
    };
    /**
     * REQUEST return http body
     * @param method
     * @param url
     * @param data
     */
    API.prototype.request = function (method, url, data) {
        if (data === void 0) { data = null; }
        var req = new http_1.HttpRequest(method, this.config.API_URL + url, data, this.getRequestOptions());
        return this.http.request(req).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * REQUEST POST multipart
     * @param url
     * @param data
     */
    API.prototype.requestPostMultiPart = function (url, data) {
        var req = new http_1.HttpRequest('POST', this.config.API_URL + url, data, this.getRequestOptionNoContentType());
        return this.http.request(req).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * REQUEST PUT multipart
     * @param url
     * @param data
     */
    API.prototype.requestPutMultiPart = function (url, data) {
        var req = new http_1.HttpRequest('PUT', this.config.API_URL + url, data, this.getRequestOptionNoContentType());
        return this.http.request(req).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * REQUEST download with header
     * @param url
     */
    API.prototype.requestDownload = function (url) {
        var req = new http_1.HttpRequest('GET', this.config.API_URL + url, null, this.getDownloadOptions());
        return this.http.request(req).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * Get method
     * @param url
     */
    API.prototype.get = function (url) {
        return this.http.get(this.config.API_URL + url, this.getRequestOptions()).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * Post method
     * @param url
     * @param data
     */
    API.prototype.post = function (url, data) {
        data = JSON.stringify(data);
        return this.http.post(this.config.API_URL + url, data, this.getRequestOptions()).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * POST multipath
     * @param url
     * @param data
     */
    API.prototype.postMultiPart = function (url, data) {
        return this.http
            .post(this.config.API_URL + url, data, this.getRequestOptionBlobNoContentType())
            .pipe(operators_1.catchError(this.handleError));
    };
    /**
     * POST multipath
     * @param url
     * @param data
     */
    API.prototype.postMultiPartJSON = function (url, data) {
        return this.http
            .post(this.config.API_URL + url, data, this.getRequestOptionNoContentType())
            .pipe(operators_1.catchError(this.handleError));
    };
    /**
     * Put method
     * @param url
     * @param data
     */
    API.prototype.put = function (url, data) {
        data = JSON.stringify(data);
        return this.http.put(this.config.API_URL + url, data, this.getRequestOptions()).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * Patch Method
     * @param url
     * @param data
     */
    API.prototype.patch = function (url, data) {
        data = JSON.stringify(data);
        return this.http.patch(this.config.API_URL + url, data, this.getRequestOptions()).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * PUT multipart
     * @param url
     * @param data
     */
    API.prototype.putMultiPart = function (url, data) {
        return this.http
            .put(this.config.API_URL + url, data, this.getRequestOptionNoContentType())
            .pipe(operators_1.catchError(this.handleError));
    };
    /**
     * Delete method
     * @param url
     */
    API.prototype["delete"] = function (url) {
        return this.http["delete"](this.config.API_URL + url, this.getRequestOptions()).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * Download file
     * @param url
     */
    API.prototype.download = function (url) {
        return this.http.get(this.config.API_URL + url, this.getDownloadOptions()).pipe(operators_1.catchError(this.handleError));
    };
    API.prototype.downloadOnlyURL = function (url) {
        return this.http.get(url, { responseType: "arraybuffer" }).pipe(operators_1.catchError(this.handleError));
    };
    /**
     * POST download
     * @param url
     * @param data
     */
    API.prototype.postDownload = function (url, data) {
        return this.http
            .post(this.config.API_URL + url, data, this.getRequestOptionBlobNoContentType())
            .pipe(operators_1.catchError(this.handleError));
    };
    /**
     * Upload File
     * @param url
     * @param file
     */
    API.prototype.uploadFile = function (url, file) {
        var req = new http_1.HttpRequest('POST', this.config.API_URL + url, file, this.getOptionsProcess());
        return this.http.request(req);
    };
    API.prototype.getOptionsProcess = function () {
        var token = this.getToken();
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                Authorization: token
            }),
            reportProgress: true
            // observe: 'events'
        };
        return httpOptions;
    };
    /**
     * Make request option no content-type for Blob
     */
    API.prototype.getRequestOptionBlobNoContentType = function () {
        var token = this.getToken();
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                Authorization: token
            }),
            responseType: 'blob'
        };
        return httpOptions;
    };
    /**
     * Make request option no content-type
     */
    API.prototype.getRequestOptionNoContentType = function () {
        var token = this.getToken();
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                Authorization: token
            })
        };
        return httpOptions;
    };
    /**
     * Make request options
     */
    API.prototype.getRequestOptions = function () {
        var token = this.getToken();
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: token
            })
        };
        return httpOptions;
    };
    /**
     * Http options for download and preview
     */
    API.prototype.getDownloadOptions = function () {
        var token = this.getToken();
        var httpOptions = {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: token,
                'Cache-Control': 'no-cache',
                Pragma: 'no-cache'
            }),
            responseType: 'blob'
        };
        return httpOptions;
    };
    /**
     * Handle Error
     * @param error
     */
    API.prototype.handleError = function (error) {
        // if (error.status === 401) {
        //   localStorage.removeItem('token');
        //   localStorage.removeItem('partyId');
        //   localStorage.removeItem('username');
        //   sessionStorage.clear();
        //   this.router.navigate(['/login']);
        // }
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code " + error.status + ", " + ("body was: " + error.message));
        }
        // return an observable with a user-facing error message
        return rxjs_1.throwError(error);
        // 'Something bad happened; please try again later.');
    };
    API = __decorate([
        core_1.Injectable()
    ], API);
    return API;
}());
exports.API = API;
