import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './pages/home/home.component';
import { MainComponent } from './layout/main/main.component';
import { LoginComponent } from './pages/login/login.component';
import { DanhsachphongbanComponent } from './pages/danhba/danhsachphongban/danhsachphongban.component';
import { ThemmoiphongbanComponent } from './pages/danhba/themmoiphongban/themmoiphongban.component';
import { ChinhsuaphongbanComponent } from './pages/danhba/chinhsuaphongban/chinhsuaphongban.component';
import { DanhsachchucvuComponent } from './pages/chucvu/danhsachchucvu/danhsachchucvu.component';
import { ThemmoichucvuComponent } from './pages/chucvu/themmoichucvu/themmoichucvu.component';
import { ChinhsuachucvuComponent } from './pages/chucvu/chinhsuachucvu/chinhsuachucvu.component';
import { DanhsachnhanvienComponent } from './pages/nhanvien/danhsachnhanvien/danhsachnhanvien.component';


const routes: Routes = [  

  { path:'login', component: LoginComponent },
  {
    path: '', component: MainComponent,
    children:[
      { path: '', component: HomeComponent },
      
      {
        path:'phongban', component: DanhsachphongbanComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'phongban-add', component: ThemmoiphongbanComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'phongban-edit', component: ChinhsuaphongbanComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'chucvu', component: DanhsachchucvuComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'chucvu-add', component: ThemmoichucvuComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'chucvu-edit', component: ChinhsuachucvuComponent,
        canActivate: [AuthGuard]
      },
      {
        path:'nhanvien', component: DanhsachnhanvienComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
